import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(
    private router: Router,
    private zone: NgZone
  ) { }

  go(url: string) {
    this.router.navigateByUrl(url);
  }
  asyncGo(url: string) {
    this.zone.run(async() => {
      await this.go(url)
    })
  }
}
