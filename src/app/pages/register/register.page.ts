import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor( private sharedService: SharedService) { }

  ngOnInit() {
  }

  register() {
    this.sharedService.go('dashboard');
  } 

}
