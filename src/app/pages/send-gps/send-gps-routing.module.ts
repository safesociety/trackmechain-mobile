import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SendGpsPage } from './send-gps.page';

const routes: Routes = [
  {
    path: '',
    component: SendGpsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SendGpsPageRoutingModule {}
