import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SendGpsPageRoutingModule } from './send-gps-routing.module';

import { SendGpsPage } from './send-gps.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SendGpsPageRoutingModule
  ],
  declarations: [SendGpsPage]
})
export class SendGpsPageModule {}
