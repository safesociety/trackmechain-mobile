import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SendGpsPage } from './send-gps.page';

describe('SendGpsPage', () => {
  let component: SendGpsPage;
  let fixture: ComponentFixture<SendGpsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendGpsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SendGpsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
